
##  What is Firewall 
Firewall is software or hardware that monitors network traffic based on the organisations policies. It monitors incoming and outgoing traffic based on set of rules.

<br />

## Firewall History

Firewall is not a new concept, it has become especially popular with the spread of the TCP/IP protocol stack due to its own nature. Since the IP protocol has the ability to intercommunicate, leaving networks with different purposes or domains (companies, universities etc.) without any control, it presents a potential risk of unauthorized access, data compromise, among other possibilities.

The first firewalls were developed in the 1980s at the American technology companies Cisco Systems and Digital Equipment Corporation. These “network layer” firewalls judged packets based on simple information such as their apparent source, destination, and connection type.Although fast and transparent, these systems were fairly easily foiled. In the early 1990s a new generation of “application layer” firewalls emerged; though more cumbersome to set up and operate, they performed a more thorough inspection. In the early 21st century, most firewalls were hybrids of these two primary types.

<br />


## How does firewalls work ?

Firewalls carefully analyze incoming traffic based on pre-established rules and filter traffic coming from unsecured or suspicious sources to prevent attacks. Firewalls guard traffic at a computer’s entry point, called ports, which is where information is exchanged with external devices.

Think of IP addresses as houses, and port numbers as rooms within the house. Only trusted people (source addresses) are allowed to enter the house (destination address) at all—then it’s further filtered so that people within the house are only allowed to access certain rooms (destination ports), depending on if they're the owner, a child, or a guest. The owner is allowed to any room (any port), while children and guests are allowed into a certain set of rooms (specific ports).

![Firewall working](https://miro.medium.com/max/1400/1*C1YfDdmeHmVGYdeXjuIoMQ.png)

<br />

## Types of Firewalls

 ### **1. Packet Filtering Firewalls**
It is the oldest and most basic type of Firewall. Operating at the network layer, they check a data packet for its source IP and destination IP, the protocol, source port, and destination port against predefined rules to determine whether to pass or discard the packet.Though this Firewalls are cheap and fast but they are very effective against malicious data packets.

### **2. Circuit-Level Gateways**
This firewall quite similar to packet filtering firewalls in that they make single check. It works at session layer. Circuit-level gateways are cost-efficient, simplistic, barely impact a network’s performance. However, their inability to inspect the content of data packets makes them an incomplete security solution on their own.

### **3. Stateful Inspection Firewalls**
Stateful inspection firewalls, and verifying and keeping track of established connections also perform packet inspection to provide better, more comprehensive security. This firewall checks for connections and source and destination IPs to determine  to which data packets can pass through. It is more secure 

### **4. Application-Level Gateways (Proxy Firewalls)**
This firewall also known as proxy firewall. They are implemented at the application layer  via a proxy device. If outsider tries to access interenet directlt, the connetion will be made to proxy firewall. After verifying it by proxy firewall it forwards to the internal device.

<br />

## Why do we need Firewalls
This era is the era of iternet. In this cyber attact is the biggest threat. Cyber criminals taking advantage of the recent rise in remote working, recognising that current circumstances make employees, devices, and systems more susceptible to attack. In first lockdown during Covid19 pandamic cyber magazin reported over 4000 new malicious COVID-related websites. To protect our data, devices, organisation from cyber attack firewalls are needed. Firewalls helps in blocking malware and preent attacks. The next generation firewalls are even more quickly can detect the malware. According to policies set by organisation the firewall works.Nowdays firewalls became an essential part of the very organision. Wthout firewall your business can face any cyber attack.

<br />

## References

https://www.parallels.com/blogs/ras/types-of-firewalls/

https://youtu.be/eO6QKDL3p1I

https://www.checkpoint.com/cyber-hub/network-security/what-is-firewall/#:~:text=A%20Firewall%20is%20a%20network,network%20and%20the%20public%20Internet.

